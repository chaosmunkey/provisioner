#!/bin/bash

# Script to run the initial setup and provisioner.
# [ "${UID}" -eq 0 ] || { echo "Script must be run as root"; exit 1; }

declare -a REQUIRED

function install_apt {
    apt update
    echo "${REQUIRED[@]}" | xargs apt install -y
}

function install_dnf {
    echo "${REQUIRED[@]}" | xargs dnf install -y
}

function install_pacman {

    echo "${REQUIRED[@]}" | xargs  pacman -Syy --noconfirm
}


for dep in "ansible" "git";
do
    type -fP "${dep}" &> /dev/null ||  { echo "Missing '${dep}'"; REQUIRED+=("${dep}"); }
done

if [ "${#REQUIRED[@]}" -ne 0 ];
then
    case "$(lsb_release -si)" in
    "Arch") install_pacman ;;
    "Debian"|"Ubuntu") install_apt ;;
    "Fedora") install_dnf ;;
    *) echo
        "Unknown distribution: $(lsb_release -si)"
        exit 2
        ;;
    esac
fi


ansible-pull -U https://gitlab.com/chaosmunkey/provisioner.git local.yaml -l "$(hostname)" -d /opt/provisioner
