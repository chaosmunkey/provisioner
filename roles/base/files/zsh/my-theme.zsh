[ -z "${PYTHON_VER}" ] && PYTHON_VER="(🐍 $(python --version 2>&1 | awk '{print $2}')"; export PYTHON_VER


if [[ "${UID}" -eq 0 ]];
then
    host_colour="red"
    user_symbol="#"
else
    host_colour="green"
    user_symbol="$"
fi


user_host="%{$terminfo[bold]$fg[${host_colour}]%}%n@%m%{$reset_color%}"
current_dir="%{$terminfo[bold]$fg[blue]%}%~%{$reset_color%}"

git_status='%{$fg[yellow]%}$(get_git_branch)%{$reset_color%}'
vi_mode='$(get_vi_mode)'

PROMPT="╭─${user_host} ${current_dir} ${git_status}
╰─${vi_mode} %B${user_symbol} %b"


# local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"


# #python_version=`/usr/bin/env python -V`
# local python_ver="%{$fg[white]%}(🐍 $(python --version 2>&1 | awk '{print $2}'))%{$reset_color%}"


# PROMPT="╭─${user_host} ${current_dir} ${python_ver} ${git_branch}
# ╰─%B${user_symbol}%b "
# RPS1="%B${return_code}%b"

# ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}‹"
# ZSH_THEME_GIT_PROMPT_SUFFIX="› %{$reset_color%}"
