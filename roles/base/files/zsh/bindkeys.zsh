# -------------------------- #
#        KEY BINDINGS        #
# -------------------------- #

# ensure VIM key bindings are used...
bindkey -v

# old emac style bindings that I'm used to...
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line
# bindkey "^n" next-history
# bindkey "^P" previous-history
bindkey "^R" history-incremental-search-backward
bindkey "^S" history-incremental-search-forward
bindkey "^[." insert-last-word
bindkey "^[B" backward-word
bindkey "^[F" forward-word
