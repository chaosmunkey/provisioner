function _set_cursor() {
    local style
    case $1 in
        reset) style=0;;
        blink_block) style=1;;
        block) style=2;;
        blink_underline) style=3;;
        underline) style=4;;
        blink_vert_line) style=5;;
        vert_line) style=6;;
    esac

    [ $style -ge 0 ] && print -n -- "\e[${style} q"
}

# starting mode...
function zle-line-init() {
    # this will default to vim insert mode for every new line
    zle -K "viins"
}

function zle-keymap-select() {
    # update keymap variable for the prompt
    [[ "$KEYMAP" == "vicmd" ]] &&
        _set_cursor block ||
        _set_cursor blink_vert_line

    zle reset-prompt
    zle -R
}

function get_vi_mode() {
    [[ "${KEYMAP}" == "vicmd" ]] &&
        MODE=" cmd " ||
        MODE=" ins "

    echo "%(?.<%{${terminfo[bold]}$fg[cyan]%}${MODE}%{${reset_color}>%}.%{<${terminfo[bold]}$fg[red]%}${MODE}%{${reset_color}>%})"
}

zle -N zle-line-init
zle -N zle-keymap-select

setopt PROMPT_SUBST


