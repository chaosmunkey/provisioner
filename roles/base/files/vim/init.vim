" ------------------------- "
"          PLUGINS          "
" ------------------------- "
" --- LIGHTLINE --- "

"let g:lightline = {'colorscheme': 'Tomorrow_Night',}

" Allow backspacing over everything in insert mode.
" set backspace=indent,eol,start
" set wildmenu

" set history=200
" set showcmd " display incomplete commands
" set ruler   " show the cursor position all the time
" set nocompatible
" set ttimeout
" set ttimeoutlen=100
if !has('gui_running')
  set t_Co=256
endif


" Line Numbers
set number relativenumber
augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

source ${XDG_CONFIG_HOME}/nvim/key-bindings.vim
source ${XDG_CONFIG_HOME}/nvim/settings.vim
source ${XDG_CONFIG_HOME}/nvim/statusline.vim
source ${XDG_CONFIG_HOME}/nvim/plugins.vim
